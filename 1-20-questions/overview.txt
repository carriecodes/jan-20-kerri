1) What are the rules for changing directories using cd? (Eg. ../../ ; changing to one directory above the current working directory and vice versa)

	- every ../ moves you up one directory. if you're at ~/dev/bloc/kerri and you run "cd ../", it will take you to ~/dev/bloc. if you run "cd ../../" from the same directory, it will take you to ~/dev, and if you run "cd ../../..", it will take you to ~, which is your home folder.

2) Could we go over examples with redirection commands and what they do? ( >, >>, | )
	- for > :
		+ open up lets-overwrite-this.txt
		+ cd into the 1-20-questions folder. we're going to use the "ps" command to get a list of processes running on the computer.
		+ run ps -ax >lets-overwrite-this.txt
		+ take a look inside lets-overwrite-this.txt now. new content! 
		+ we can see that the > command takes output and redirects it from the command line to a file, where it overwrites the existing contents. 
	- for >> :
		+ open up lets-add-to-this.txt
		+ run ps -ax >>lets-add-to-this.txt
		+ take a look inside lets-add-to-this.txt now. new content in addition to the old!
		+ we can see that the > command takes output and redirects it from the command line to a file, where it *adds to* the existing contents.
	- for | :
		+ basically you are passing the results of what's on the left side of the pipe to a command on the right side of the pipe.
		+ run ps -ax | grep [something] (we may have to play with the actual value here since you're on windows and i'm on mac, but we'll find a way to demo this)

3) Definitions of script, string, variable, “piped”?
	- script: a sequence/series of instructions 
	- string: a representation of text.
		+ "this is a string!"
		+ "this is a string about a string!"
	- variable: a value that can change, represented by a symbol as you would in math.
		+ ex: 
			* var x = 9
			* var name = "Kerri"
			* var cat = { name: "Peppercorn", age: 3 }
	- piped: specific to the command line, it means passing the result of one command to another command as we did above.

4) How is Command connected to Ruby? I noticed ruby in the following: PATH=/home/ccuser/.bin:/home/ccuser/.gem/ruby/2.0.0/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin
	- it's connected in that you can run ruby files in the command line (as long as ruby is installed), but this isn't specific to ruby. you can also run java, javascript, python, etc., as long as you have their environments set up.

5) Could we go over some of the variables such as PATH, PWD, PS1?
	- PATH 
		+ basically this tells your computer (including the command line) where certain executables live. (an executable is something like ruby.exe.) this way, you can run a file by typing "ruby filename.rb" rather than typing "entire/path/to/ruby.exe filename.rb".
	- pwd
		+ this is actually a command that prints out the current directory that you are in. 
	- PS1
		+ the prompt string in the command line shell. run PS1="hey there : " and see what happens. 
